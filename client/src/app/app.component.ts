import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ArticleService } from './articles/article.service';
import ExternalArticle from './articles/models/external-article';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public externalArticles$: Observable<ExternalArticle[]>;
  public articleAbstract$: Observable<string>;

  constructor(private articleService: ArticleService) {
    this.externalArticles$ = this.articleService.getArticles();
    this.articleAbstract$ = this.articleService.articleAbstract$;
  }
}
