export default interface ExternalArticle {
    type: 'pubmed' | 'omim' | 'hgmd';
    id: string;
}
