import { Component, Input } from '@angular/core';
import ExternalArticle from '../models/external-article';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss']
})
export class ArticleListComponent {

  @Input()
  public articles: ExternalArticle[] = [];
}
