import { Component, Input } from '@angular/core';
import { ArticleService } from '../article.service';
import ExternalArticle from '../models/external-article';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent {

  @Input()
  public article?: ExternalArticle;

  constructor(private articleService: ArticleService) {

  }

  public handleClick(article: ExternalArticle): void {
    this.articleService.getArticleAbstract(article.id);
  }
}
