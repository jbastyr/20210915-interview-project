import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-article-abstract',
  templateUrl: './article-abstract.component.html',
  styleUrls: ['./article-abstract.component.scss']
})
export class ArticleAbstractComponent {
  @Input()
  public abstract?: String;
}
