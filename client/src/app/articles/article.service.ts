import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import ExternalArticle from './models/external-article';

@Injectable()
export class ArticleService {

    private readonly base: string;

    private readonly articleAbstractSubject: Subject<string> = new Subject<string>();
    
    private readonly jsonHeaders: HttpHeaders = new HttpHeaders({
        'Accept': 'application/json'
    });

    private readonly textHeaders: HttpHeaders = new HttpHeaders({
        'Accept': 'text/plain'
    });

    public readonly articleAbstract$: Observable<string> = this.articleAbstractSubject.asObservable();

    constructor(
        private http: HttpClient,
    ) {
        this.base = document.getElementsByTagName('base')[0].href;
    }

    public getArticles(): Observable<ExternalArticle[]> {
        return this.http.get<ExternalArticle[]>(
            `${this.base}api/articles`,
            { headers: this.jsonHeaders }
        );
    }

    public getArticleAbstract(id: string): void {
        this.http.get(
            `${this.base}api/articles/${id}/abstract`,
            {
                headers: this.textHeaders,
                responseType: 'text'
            }
        ).subscribe(
            abstract => this.articleAbstractSubject.next(abstract),
            error => this.articleAbstractSubject.next(`Error retrieving abstract: [${error?.error}]`)
            );
    }

}
