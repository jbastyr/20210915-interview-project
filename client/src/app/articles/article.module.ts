import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleAbstractComponent } from './article-abstract/article-abstract.component';
import { ArticleComponent } from './article/article.component';
import { ArticleListComponent } from './article-list/article-list.component';
import { ArticleService } from './article.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    ArticleListComponent,
    ArticleComponent,
    ArticleAbstractComponent
  ],
  exports: [
    ArticleListComponent,
    ArticleAbstractComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    ArticleService
  ]
})
export class ArticleModule { }
