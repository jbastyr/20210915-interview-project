import ExternalArticle from '../models/external-article';
import got from 'got';
import $ from 'cheerio';

const pubmedBaseUrl: string = 'https://pubmed.ncbi.nlm.nih.gov';
const pubmedAbstractSelector: string = 'div#abstract > div#enc-abstract > p'

export async function getArticleAbstract(article: ExternalArticle): Promise<string> {
    if (article?.type !== 'pubmed') {
        throw new Error('Unsupported article type');
    }

    const response = await got(`${pubmedBaseUrl}/${article.id}`);

    if (response.statusCode != 200 || !response.body) {
        throw new Error('Article not found');
    }

    const abstract = $(pubmedAbstractSelector, response.body).text();

    if (!abstract) {
        throw new Error('Abstract for article not found');
    }

    return abstract;
}
