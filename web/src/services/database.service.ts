import * as mongodb from 'mongodb';
import ExternalArticle from '../models/external-article';

export const collections: { articles?: mongodb.Collection<ExternalArticle> } = {};

export async function openDbConnection(): Promise<void> {

    const client = new mongodb.MongoClient(process.env.DB_CONN_STRING);
    
    await client.connect();

    const db = client.db(process.env.DB_NAME);

    const articles = db.collection<ExternalArticle>(process.env.EXTERNAL_ARTICLE_COLLECTION_NAME);

    collections.articles = articles;
}
