import express from 'express';
import { articlesRouter } from './routes/articles.router';
import { openDbConnection } from './services/database.service';
import * as dotenv from 'dotenv';

const app = express();
dotenv.config()

const port = process.env.HTTP_PORT;
const baseRoute = process.env.BASE_ROUTE;
const baseApiRoute = baseRoute + process.env.API_ROUTE;

openDbConnection()
    .then(() => {
        app.use(`${baseApiRoute}/articles`, articlesRouter);
        app.use(`${baseRoute}`, express.static('www/'));

        app.listen(port, () => {
            console.log(`server listening on http://localhost:${port}`)
        })
    })
    .catch((error: Error) => {
        console.error('Database connection failed: ', error);
        process.exit();
    });