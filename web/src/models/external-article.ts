import { ObjectId } from 'mongodb';

export default interface ExternalArticle {
    type: 'pubmed' | 'omim' | 'hgmd';
    id: string;
    _id: ObjectId;
}
