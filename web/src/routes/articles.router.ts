import express, { Request, Response } from 'express';
import { collections } from '../services/database.service';
import { getArticleAbstract } from '../services/abstract.service';

export const articlesRouter = express.Router();

articlesRouter.use(express.json());

articlesRouter.get('/', async (req: Request, res: Response) => {
    try {
        const articles = await collections.articles.find().toArray();

        res.status(200).send(articles);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

articlesRouter.get('/:id/abstract', async (req: Request, res: Response) => {
    const id = req?.params?.id;
    
    try {
        // make sure requested article exists in our db
        const query = { id: id };
        const article = await collections.articles.findOne(query);

        if(!article) {
            res.status(404).send('Article not found');
            return;
        }

        // query pubmed for the article
        const abstract = await getArticleAbstract(article);

        res.status(200).send(abstract);
    } catch (error) {
        res.status(500).send(error.message);
    }
});